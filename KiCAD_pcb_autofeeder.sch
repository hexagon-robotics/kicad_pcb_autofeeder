EESchema Schematic File Version 4
LIBS:KiCAD_pcb_autofeeder-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR02
U 1 1 5CF002CE
P 4400 3000
F 0 "#PWR02" H 4400 2850 50  0001 C CNN
F 1 "+5V" H 4415 3173 50  0000 C CNN
F 2 "" H 4400 3000 50  0001 C CNN
F 3 "" H 4400 3000 50  0001 C CNN
	1    4400 3000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5CF0032F
P 4750 3350
F 0 "#PWR01" H 4750 3100 50  0001 C CNN
F 1 "GND" H 4755 3177 50  0000 C CNN
F 2 "" H 4750 3350 50  0001 C CNN
F 3 "" H 4750 3350 50  0001 C CNN
	1    4750 3350
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4001 D1
U 1 1 5CF00485
P 9000 3050
F 0 "D1" V 8954 3129 50  0000 L CNN
F 1 "1N4001" V 9045 3129 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 9000 2875 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 9000 3050 50  0001 C CNN
	1    9000 3050
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 EN1
U 1 1 5CF00540
P 7400 2550
F 0 "EN1" H 7320 2225 50  0000 C CNN
F 1 "Conn_01x02" H 7320 2316 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7400 2550 50  0001 C CNN
F 3 "~" H 7400 2550 50  0001 C CNN
	1    7400 2550
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 SWTCH1
U 1 1 5CF0060E
P 7400 3800
F 0 "SWTCH1" H 7320 3475 50  0000 C CNN
F 1 "Conn_01x03" H 7320 3566 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7400 3800 50  0001 C CNN
F 3 "~" H 7400 3800 50  0001 C CNN
	1    7400 3800
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 N20
U 1 1 5CF008CA
P 8400 3100
F 0 "N20" H 8320 2775 50  0000 C CNN
F 1 "Conn_01x02" H 8320 2866 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8400 3100 50  0001 C CNN
F 3 "~" H 8400 3100 50  0001 C CNN
	1    8400 3100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 SERVO1
U 1 1 5CF008F2
P 6500 3100
F 0 "SERVO1" H 6420 2775 50  0000 C CNN
F 1 "Conn_01x03" H 6420 2866 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6500 3100 50  0001 C CNN
F 3 "~" H 6500 3100 50  0001 C CNN
	1    6500 3100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 IN1
U 1 1 5CF00BF7
P 5150 2600
F 0 "IN1" H 5070 2175 50  0000 C CNN
F 1 "Conn_01x04" H 5070 2266 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5150 2600 50  0001 C CNN
F 3 "~" H 5150 2600 50  0001 C CNN
	1    5150 2600
	-1   0    0    1   
$EndComp
Text Notes 4350 2600 0    39   ~ 0
FEEDBACK
Text Notes 4250 2700 0    39   ~ 0
SERVO_SIGNAL
Text Notes 7750 3700 0    39   ~ 0
NO
Text Notes 7750 3800 0    39   ~ 0
NC
Text Notes 7750 3900 0    39   ~ 0
COM
Wire Wire Line
	5000 2800 4400 2800
Wire Wire Line
	4400 2800 4400 3000
Wire Wire Line
	4750 3350 4750 2900
Wire Wire Line
	5350 2700 5350 2900
Wire Wire Line
	5350 2900 4750 2900
Wire Wire Line
	5000 2800 5000 2600
Wire Wire Line
	5000 2600 5350 2600
Wire Wire Line
	5350 2500 6700 2500
Wire Wire Line
	6700 2500 6700 3000
Wire Wire Line
	6300 2600 5350 2600
Connection ~ 5350 2600
Wire Wire Line
	6300 2600 6300 3100
Wire Wire Line
	6300 3100 6700 3100
Wire Wire Line
	6700 3200 5350 3200
Wire Wire Line
	5350 3200 5350 2900
Connection ~ 5350 2900
Wire Wire Line
	5350 2400 7100 2400
Wire Wire Line
	7100 2400 7100 3600
Wire Wire Line
	7100 3600 7600 3600
Wire Wire Line
	7600 3600 7600 3700
Wire Wire Line
	7600 3800 8600 3800
Wire Wire Line
	8600 3800 8600 3100
Wire Wire Line
	9000 3200 8750 3200
Wire Wire Line
	8750 3200 8750 3100
Wire Wire Line
	8750 3100 8600 3100
Connection ~ 8600 3100
Wire Wire Line
	8600 3000 8750 3000
Wire Wire Line
	8750 3000 8750 2900
Wire Wire Line
	8750 2900 9000 2900
Wire Wire Line
	7600 2450 8750 2450
Wire Wire Line
	8750 2450 8750 2900
Connection ~ 8750 2900
Wire Wire Line
	7600 2550 7600 3100
Wire Wire Line
	7600 3100 6700 3100
Connection ~ 6700 3100
Wire Wire Line
	6700 3200 6700 4050
Wire Wire Line
	6700 4050 7600 4050
Wire Wire Line
	7600 4050 7600 3900
Connection ~ 6700 3200
$EndSCHEMATC
